$(document).ready(function() {
    
    /*SCRIPT PARA USAR LA LIBRERÍA SELECT2*/
    if($(".select").length>0){
       
        $(".select").select2();
    }
    

    /*SCRIPT PARA USAR LA LIBRERÍA dataTable*/
    if($("#table").length>0){

        $('#table').DataTable();
    }

    /*SCRIPT PARA USAR LA LIBRERÍA CKEDITOR*/
    if($("textarea").length>0){
        CKEDITOR.replace( 'content' );
    }

    $.mockjax({
        url : '/foo/bar.html',
        isTimeout:false,
        responseText : 'Server Response Emulated',
        
        


       
      })
});

$(document).on('click','#enviar', function(event){

    $("#pedirCita").validate({
    
        onfocusout: true,
        errorClass: "is-invalid",
        ignore: [],

        rules: {
            name: {
                required: true,
                minlength: 2,
                maxlength: 18,
            },
            lastname: {
                required: true,
                minlength: 5,
                maxlength: 25,
            },
            email: {
                required: true,
                minlength: 5,
                maxlength: 25,
                email: true,
            },
            numberphone: {
                required: true,
                minlength: 5,
                maxlength: 10,
                number: true,
            },

            states: {
                required: true,
            },

            content: {
                required: function() 
                {
                    CKEDITOR.instances.content.updateElement();
                }
            }
        },

        messages: {
            name: {
                required: "El nombre es obligatorio",
                minlength:"El mínimo de carácteres permitido es 5",
                maxlength:"El máximo de carácteres permitido es 18",
            },
            lastname: {
                required: "El apellido es obligatorio",
                minlength:"El mínimo de carácteres permitido es 5",
                maxlength:"El máximo de carácteres permitido es 25",
            },
            email: {
                required: "El email es obligatorio",
                email: "Tienes que escribir un email valido",
                minlength:"El mínimo de carácteres permitido es 5",
                maxlength:"El máximo de carácteres permitido es 25"
            },
            numberphone: {
                required: "El teléfono es obligatorio",
                minlength:"El mínimo de carácteres permitido es 5",
                maxlength:"El máximo de carácteres permitido es 10",
                number: "Solo puede introducir números"
            },

            states: {
                required: "Debe seleccionar al menos una terapia"
            },

            content: {
                required: "Debe añadir un comentario"
            }
        }
    });

    if($("#pedirCita").valid()){

        event.preventDefault();

        //Variable para los datos del texto
        var textdata = CKEDITOR.instances.content.getData();

        //Metemos todo el formulario dentro de un objeto
        var formData = new FormData($("#pedirCita")[0]);

        //Podemos añadirle más datos "manualmente" con append (como la de ckeditor)
        //el "content" (en este caso) machacará el previo que habrá generado el html.
        formData.append("content", textdata);

        for(var pair of formData.entries()) {
            console.log(pair[0]+ ', '+ pair[1]);
        };
         
        $.ajax({
            type: 'post',
            url: "/foo/bar.html",
            dataType: 'text',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (responseText) {

                $("#result").html(responseText);
       
                      
            },
    
            error: function(){
                $("#result").html("error");
                
                
            }
        });
    
        /*return false;*/
    };
});