<!DOCTYPE html>

<html lang="es">

	<head>
		<!--CÓDIGO TECLADO-->
		<meta charset="UTF-8">
		<!--TÍTULO DE LA PÁGINA-->
		<title>Página de Inicio</title>
		<!--RESPONSIV-->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        
        <!--CÓDIGO BOOTSTRAP-->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
		<!--CODIGO FUENTE DE GOOGLE-->
		<link href="https://fonts.googleapis.com/css?family=Black+Han+Sans" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">
		<!--CSS-->
		<link rel="stylesheet" href="css/app.css">
	</head>
	
	<body>
		<!--CAJA PRINCIPAL-->
		<div class="container-fluid">
			<!--CABECERA-->
			<div class="row header">
				
				<nav class="navbar w-100 navbar-expand-lg navbar-light col-sm-12">
						
						<!--LOGOTIPO-->
						<div class="logo">
									<img src="imagenes/logo.png" alt="LOGO">
						</div>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>
						<div class="collapse navbar-collapse" id="navbarNavDropdown">
							<ul class="navbar-nav ml-auto">
							  <li class="nav-item active">
								<a href="http://fisioterapiajesusbolivar.com" title="Inicio">Inicio </a>
							  </li>
							  <li class="nav-item">
								<a href="http://fisioterapiajesusbolivar.com/tratamientos"title="Tratamientos">Tratamientos</a>
							  </li>
							  <li class="nav-item">
								<a href="http://fisioterapiajesusbolivar.com/centro" title="Centro">Centro</a>
							  </li>
							  <li class="nav-item">
								<a href="http://fisioterapiajesusbolivar.com/personal" title="Personal">Personal</a>
							  </li>
							  <li class="nav-item">
								<a href="http://fisioterapiajesusbolivar.com/grupos" title="Grupos">Grupos</a>
							  </li>
							  <li class="nav-item">
								<a href="http://fisioterapiajesusbolivar.com/sesiones" title="Sesiones">Sesiones</a>
							  </li>
							  <li class="nav-item">
								<a href="http://fisioterapiajesusbolivar.com/contacto" title="Contacto">Contacto</a>
							  </li>
							</ul>
						</div>
					</nav>
				</div>
					<!-- Versión anterior del menu 
						
						<a href="http://fisioterapiajesusbolivar.com/tratamientos"title="tratamientos"</a>
						<a href="http://fisioterapiajesusbolivar.com/centro"title="centro"<button type="button" class="btn btn-success">El centro</button></a>
						<a href="http://fisioterapiajesusbolivar.com/personal"title="personal"<button type="button" class="btn btn-danger">El personal</button></a>
						<a href="http://fisioterapiajesusbolivar.com/grupos"title="grupos"<button type="button" class="btn btn-warning">Grupos</button></a>
						<a href="http://fisioterapiajesusbolivar.com/sesiones"title="sesiones"<button type="button" class="btn btn-info">Sesiones</button></a>
						<a href="http://fisioterapiajesusbolivar.com/contacto"title="contacto"<button type="button" class="btn btn-light">Contacto</button></a>
					-->
				
			