<!--SLIDER-->
<div class="row slider">
    <!--CAJA IMAGEN-->
    <div id="carouselExampleSlidesOnly " class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
            <img class="d-block w-100" src="imagenes/fisioterapia-manual.jpg" alt="fisioterapia-manual">
            </div>
            <div class="carousel-item">
            <img class="d-block w-100" src="imagenes/centro-fisioterapia.jpg" alt="centro-fisioterapia">
            </div>
            <div class="carousel-item">
            <img class="d-block w-100" src="imagenes/fisioterapia.jpg" alt="paciente-fisioterapia">
            </div>
        </div>
    </div>
    <!--CAJA TEXTO-->
    <div class="slider-text">
        <h1>
            Si te dejas<br> en buenas manos, <br>tu <b>dolor</b> desaparecerá		
        </h1>
    </div>