        <!--FOOTER-->
        <div class="row footer">
            <!--COPYRIGHT-->
            <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                <p>Copyright © 2015 fisioterapiajesusbolivar.com</p>
                <br>
                <p>Dirección: <b>C/ Ignasi Ferretjans, 3</b></p>
                <br>
                <p>Contacto: <b>871930865</b></p>	
            </div>
            <!--REDES SOCIALES-->
            <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                <img class="redes"src="imagenes/if_social-facebook_216078.png" alt="facebook">
                <img class="redes"src="imagenes/if_Twitter_Color_1435159.png" alt="twitter">
                <img class="redes"src="imagenes/if_40-google-plus_104464.png" alt="google">
                <br>
                <p>Designed by dpicode.com.</p>
                <br>
                <p>Privacidad y Protección de datos</p>	
            </div>
        </div>	

    </div>	
    <!--CÓDIGO BOOTSTRAP-->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    </body>	

</html>